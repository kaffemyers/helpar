# helpar

HELp section PARser written in gawk

## Summary

By formatting the top of your script in a pre-defined way, output
a help section that is aware of your terminal size. Look inside
this script for an example.

This is compatible with any script that uses hash (#) for comments
(Python, bash, sh, awk...)

An empty line will be treated as end of paragraph. Otherwise,
it will re-wrap the content to fit.

Special key word BASENAME will be converted to the name of the script you are parsing. :)

## Installation

Will be part of [scalr](https://gitlab.com/kaffemyers/scalr) repository in the end. For now,
just put the files in the folder structure of this git repository, and everything should work
just fine. :)

```shell
git clone https://gitlab.com/kaffemyers/helpar
cd helpar
sudo mkdir --parents /opt
sudo cp --recursive opt/helpar /opt/.
sudo cp usr/local/bin/helpar /usr/local/bin/helpar
```
